<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header();

?>

<!-- Commented out for now, to evaluate if are necessary for the
sidebar placement. -->
<!-- Currently using a layout with sovereign sections. Each section has a
wrapper that takes all the width (and can be fed a background that has the same
width) and a section content which is a bootstrap container and can be manipulated
width-wise -->
<!-- <div id="primary" class="content-area"> -->
 <!-- <main id="main" class="site-main" role="main"> -->
  
  <?php
  /**
   * Set up the paged variable for correct paging with the custom query.
   */
  if(get_query_var('paged')){
    $paged=get_query_var('paged');
  }
  else if(get_query_var('page')){
    $paged=get_query_var('page');
  }
  else{
    $paged=1;
  }

  /**
   * Setup the aggregate custom query to include posts from all the desired custom post types. 
   */
  $aggregate_query_args=array('post_type'=>array('apo_koinou_article', 'apo_koinou_news', 'apo_koinou_action'), 'posts_per_page'=>15, 'paged'=>$paged);
  $aggregate_query=new WP_Query($aggregate_query_args);
  ?>
 
<!-- The begin of the posts section -->
<div class="post-section-wrapper">
  <section id="post-content-section" class="container">
    
  <!-- Here lies the page title. -->
  <div class="apo-koinou page-title l1-wrapper">
    <div class="apo-koinou page-title l2-wrapper">
      <a href="<?php echo get_site_url(); ?>">
        <h1 class="apo-koinou page-title page-title-text">
          <?php bloginfo('name'); ?>
        </h1>
      </a>
    </div>
  </div>

  <?php
  if( $aggregate_query->have_posts()): ?>
  <?php if ( is_home() && ! is_front_page() ) : ?>
    <header>
      <h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
    </header>
  <?php endif; ?>

  <?php
    /*
     * Start the Loop.
     */
    while($aggregate_query->have_posts()):
      $aggregate_query->the_post();
  ?>

  <article class="apo-koinou article-marquee">

    <!-- Here lies the article featured image -->
    <div class="apo-koinou archive article-title l1-wrapper">
      <div class="apo-koinou archive article-title l2-wrapper">
        <a href="<?php the_permalink(); ?>">
          <?php
          if(has_post_thumbnail()): ?>
            <img class="apo-koinou archive article-title featured-image" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), "archive_page_thumb_crop"); ?>">
          <?php
          else:
          ?>
            <img class="apo-koinou archive article-title featured-image" src="<?php echo wp_get_attachment_image_src(87, "archive_page_thumb_crop")[0]; ?>">
          <?php
          endif;
          ?>
        </a>
      </div>
    </div>
    
    <div class="apo-koinou article-marquee textual-elements">
      <!-- Here lies the article title -->
      <div class="apo-koinou archive article-title l1-wrapper">
        <div class="apo-koinou archive article-title l2-wrapper">
          <h2 class="apo-koinou archive article-title archive article-title-text">
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
          </h2>
        </div>
      </div>

      <!-- Here lies the article date -->
      <div class="apo-koinou archive article-title l1-wrapper">
        <div class="apo-koinou archive article-title l2-wrapper">
          <span class="apo-koinou archive article-title date-text">
            <?php the_time('j F Y'); ?>
          </span>
        </div>
      </div>

      <!-- Here lies the category indicator for the article -->
      <div class="apo-koinou archive category-indicator l1-wrapper">
        <div class="apo-koinou archive category-indicator l2-wrapper">
          <div class="apo-koinou archive category-indicator indicator-content <?php $key=array_keys(get_the_taxonomies(get_the_ID()))[0]; echo get_the_terms(get_the_ID(), $key)[0]->slug; ?>">
            <div class="apo-koinou archive category-indicator little-square"></div>
            <div class="apo-koinou archive category-indicator label-rectangle">
              <?php
              $args=array(
                  "template"=>'%2$l',
                  "term_template"=>'<span>%2$l</span>'
              );
              echo get_the_taxonomies(get_the_ID(), $args)[$key];
              ?>
            </div>
          </div>
        </div>
      </div>

      <!-- Here lies the article excerpt-->
      <div class="apo-koinou archive article-excerpt l1-wrapper">
        <div class="apo-koinou archive article-excerpt l2-wrapper">
          <span class="apo-koinou archive article-excerpt excerpt-text">
            <?php the_excerpt(); ?>
          </span>
        </div>
      </div>

      <!-- Here lies the article edit link only for logged in users -->
      <?php
      if(is_user_logged_in()):
      ?>
      <div class="apo-koinou archive article-edit-link l1-wrapper">
        <div class="apo-koinou archive article-edit-link l2-wrapper">
          <div class="apo-koinou archive article-edit-link edit-link">
            <?php edit_post_link(); ?>
          </div>
        </div>
      </div>
      <?php
      endif;
      ?>
    </div>
  </article>
  <div class="apo-koinou archive article-separator-ball">
    <img src="<?php echo wp_get_attachment_image_src(92, 'thumbnail')[0]; ?>">
  </div>
  <?php
  endwhile;

// If no content, include the "No posts found" template.
else :
  //get_template_part( 'template-parts/content', 'none' );
endif;
/**
 * Handle the pagination issue on a static page with a custom query.
 */
wp_reset_postdata();
previous_posts_link('προηγουμενο');
next_posts_link('επομενο', $aggregate_query->max_num_pages);
?>
  </section>
</div>
    
  <!-- Commented out for now, to evaluate if are necessary for the
  sidebar placement. -->
  <!-- </main><!-- .site-main -->
	<!-- </div><!-- .content-area -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
