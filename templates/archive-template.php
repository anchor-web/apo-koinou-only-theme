<?php
/**
 * Set up the current archive/category/tag loop
 */
if(have_posts()):
  while(have_posts):
    the_post();
?>
<!-- This section contains the form of an article's preview in an archive-type page
whether it be category page or a tag page. -->
<div class="apo-koinou content-area super-container">
  <main id="apo-koinou-content-area-container" class="apo-koinou section container">
    
    <!-- Here lies the title of the page. -->
    <header class="apo-koinou page-title l1-wrapper">
      <div class="apo-koinou page-title l2-wrapper">
        <div class="apo-koinou page-title page-title-text">
          <h1><?php //wp_get_the_title(); ?></h1>
        </div>
      </div>
    </header>
    
    <!-- Here lie the breadcrumbs. -->
    <div class="apo-koinou breadcrumbs l1-wrapper">
      <div class="apo-koinou breadcrumbs l2-wrapper">
        <span class="apo-koinou breadcrumbs breadcrumbs-text"></span>
      </div>
    </div>
    
    <!-- Here lies the article elements -->
    <div class="apo-koinou article-area l1-wrapper">
      <div class="apo-koinou article-area l2-wrapper">
        <article class="apo-koinou article-area article">
          
          <!-- Here lies the featured article image -->
          <div class="apo-koinou archive article-title l1-wrapper">
            <div class="apo-koinou archive article-title l2-wrapper">
              <img class="apo-koinou archive article-title featured-image" src="<?php get_the_post_thumbnail_url(); ?>">
            </div>
          </div>
          
          <!-- Here lies the article date -->
          <div class="apo-koinou archive article-title l1-wrapper">
            <div class="apo-koinou archive article-title l2-wrapper">
              <div class="apo-koinou archive article-title date-text">
                <?php get_the_date()?>
              </div>
            </div>
          </div>
          
          <!-- Here lies the article category-->
          <div class="apo-koinou article-category l1-wrapper">
            <div class="apo-koinou article-category l2-wrapper">
              <div class="apo-koinou article-category category-box">
                
              </div>
            </div>
          </div>
          
          <!-- Here lies the article title -->
          <div class="apo-koinou archive article-title l1-wrapper">
            <div class="apo-koinou archive article-title l2-wrapper">
              <div class="apo-koinou archive article-title title-text">
                
              </div>
            </div>
          </div>
          
          <!-- Here lies the author -->
          <div class="apo-koinou article-author l1-wrapper">
            <div class="apo-koinou article-author l2-wrapper">
              <div class="apo-koinou article-author author-text">
                
              </div>
            </div>
          </div>
          
          <!-- Here lies the article excerpt -->
          <div class="apo-koinou article-excerpt l1-wrapper">
            <div class="apo-koinou article-excerpt l2-wrapper">
              <div class="apo-koinou article-excerpt excerpt-text">
                <?php the_excerpt(); ?>
              </div>
            </div>
          </div>  
        </article>
      </div>
    </div>
    <?php if(is_user_logged_in()){edit_article_link();} ?>
    
    <!-- Here lie the pagination details -->
    <div class="apo-koinou pagination-box l1-wrapper">
      <div class="apo-koinou pagination-box l2-wrapper">
        <div class="apo-koinou pagination-box">

        </div>
      </div>
    </div>
  </main>
</div>
<?php
  endwhile;
endif;
echo get_footer();