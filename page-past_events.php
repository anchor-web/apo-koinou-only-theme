<?php

get_header();

/**
 * Set up the paged variable for correct paging with the custom query.
 */
if(get_query_var('paged')){
  $paged=get_query_var('paged');
}
else if(get_query_var('page')){
  $paged=get_query_var('page');
}
else{
  $paged=1;
}

/**
 * Create the WP_Query to feed the page with the appropriate data.
 */

$args=array(
    'post_type'=>'apo_koinou_action',
    'meta_key'=>'action_date',
    'meta_value'=>date('Ymd'),
    'meta_type'=>'DATE',
    'meta_compare'=>'<',
    'posts_per_page'=>15,
    'paged'=>$paged
    );
$past_events_query=new WP_Query($args);
?>
  
<!-- The begin of the posts section -->
<div class="post-section-wrapper">
  <section id="post-content-section" class="container">

  <!-- Here lies the page title complex (title and breadcrumbs). -->
  <div class="apo-koinou page-title l1-wrapper">
    <div class="apo-koinou page-title l2-wrapper">

      <!-- Here lies the title text. -->
      <a class="apo-koinou page-title page-title-link" href="<?php echo get_page_link(62); ?>">
        <h3 class="apo-koinou breadcrumbs breadcrumbs-text">
          <?php echo get_the_title(62); ?>
        </h3>
      </a>

      <!-- Here lie the breadcrumb elements. -->
      <span class="apo-koinou breadcrumbs breadcrumbs-symbol">
        <?php echo ' >> '; ?>
      </span>
      <h1 class="apo-koinou page-title page-title-text">
        <?php echo get_the_title(8); ?>
      </h1>
    </div>
  </div>

  <?php
if($past_events_query->have_posts()):
  while($past_events_query->have_posts()):
    $past_events_query->the_post();
?>
    
    <!-- Here lies the article elements -->
    <article class="apo-koinou article-marquee">
    
    <!-- Here lies the article featured image -->
    <div class="apo-koinou archive article-title l1-wrapper">
      <div class="apo-koinou archive article-title l2-wrapper">
        <a href="<?php the_permalink(); ?>">
          <?php
          if(has_post_thumbnail()): ?>
            <img class="apo-koinou archive article-title featured-image" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), "archive_page_thumb_crop"); ?>">
          <?php
          else:
          ?>
            <img class="apo-koinou archive article-title featured-image" src="<?php echo wp_get_attachment_image_src(87, "archive_page_thumb_crop")[0]; ?>">
          <?php
          endif;
          ?>
        </a>
      </div>
    </div>
    
    <div class="apo-koinou article-marquee textual-elements">
      <!-- Here lies the article title -->
    <div class="apo-koinou archive article-title l1-wrapper">
      <div class="apo-koinou archive article-title l2-wrapper">
        <h2 class="apo-koinou archive article-title archive article-title-text">
          <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h2>
      </div>
    </div>
          
    <!-- Here lies the action scheduled date -->
    <div class="apo-koinou archive article-title l1-wrapper">
      <div class="apo-koinou archive article-title l2-wrapper">
        <span class="apo-koinou archive article-title date-text">
          <?php
          $greekMonths = array('Ιανουαρίου','Φεβρουαρίου','Μαρτίου','Απριλίου','Μαΐου','Ιουνίου','Ιουλίου','Αυγούστου','Σεπτεμβρίου','Οκτωβρίου','Νοεμβρίου','Δεκεμβρίου');
          $scheduled_date=new DateTime(get_field('action_date'));
          $scheduled_date=$scheduled_date->format('Y-m-d');
          echo date('j', strtotime($scheduled_date)).' '.$greekMonths[date('m', strtotime($scheduled_date))-1]. ' '. date('Y', strtotime($scheduled_date));
          ?>
        </span>
      </div>
    </div>
          
    <!-- Here lies the category indicator for the article -->
    <div class="apo-koinou archive category-indicator l1-wrapper">
      <div class="apo-koinou archive category-indicator l2-wrapper">
        <div class="apo-koinou archive category-indicator indicator-content <?php $key=array_keys(get_the_taxonomies(get_the_ID()))[0]; echo get_the_terms(get_the_ID(), $key)[0]->slug; ?>">
            <div class="apo-koinou archive category-indicator little-square"></div>
          <div class="apo-koinou archive category-indicator label-rectangle">
            <?php
            $args=array(
                "template"=>'%2$l',
                "term_template"=>'<span>%2$l</span>'
            );
            echo get_the_taxonomies(get_the_ID(), $args)[$key];
            ?>
          </div>
        </div>
      </div>
    </div>
          
    <!-- Here lies the article excerpt -->
    <div class="apo-koinou archive article-excerpt l1-wrapper">
      <div class="apo-koinou archive article-excerpt l2-wrapper">
          <span class="apo-koinou archive article-excerpt excerpt-text">
          <?php the_excerpt(); ?>
          </span>
        </div>
      </div>
        
    <!-- Here lies the article edit link only for logged in users -->
    <?php
    if(is_user_logged_in()):
    ?>
    <div class="apo-koinou archive article-edit-link l1-wrapper">
      <div class="apo-koinou archive article-edit-link l2-wrapper">
        <div class="apo-koinou archive article-edit-link edit-link">
          <?php edit_post_link(); ?>
        </div>
      </div>
    </div>
<?php
    endif;
    ?>
    </div>
  </article>
  <div class="apo-koinou archive article-separator-ball">
    <img src="<?php echo wp_get_attachment_image_src(92, 'thumbnail')[0]; ?>">
  </div>
<?php
  endwhile;

// If no content, include the "No posts found" template.
else :
  //get_template_part( 'template-parts/content', 'none' );
endif;
?>

  <!-- Here lie the pagination details -->
  <div class="apo-koinou pagination-box l1-wrapper">
    <div class="apo-koinou pagination-box l2-wrapper">
      <div class="apo-koinou pagination-box">
      <?php
      /**
       * Show the pagination links.
       * This is the appropriate way for a custom wp_query in a static page.
       */
      wp_reset_postdata();
      previous_posts_link('προηγουμενο');
      next_posts_link('επομενο', $events_participations_query->max_num_pages);
      ?>
      </div>
    </div>
  </div>
  </section>
</div>

<?php echo get_footer();