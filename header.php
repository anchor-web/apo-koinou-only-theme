<?php

/**
 * The template for displaying the header
 * 
 * Displays all of the head element and everything up until the "site-content" div.
 * 
 * @package WordPress
 * @subpackage apo_koinou_theme
 * @since Apo Koinou 0.1
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php if (is_singular() && pings_open(get_queried_object())): ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url')?>">
    <?php endif; ?>
    <?php wp_head(); ?>
  </head>
  
  <body>
    <header>
      <div>
        <?php //if(has_nav_menu('main_nav_menu')): ?>
          <div> <!-- aria-label="" -->
            <?php get_template_part('parts/nav', 'main'); ?>
          </div>
        <?php //endif; ?>
      </div>
    </header>