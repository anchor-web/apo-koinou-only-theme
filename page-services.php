<?php

get_header();

?>

<!-- The begin of the posts section -->
<div class="post-section-wrapper">
  <section id="post-content-section" class="container">

  <!-- Here lies the page title. -->
  <div class="apo-koinou page-title l1-wrapper">
    <div class="apo-koinou page-title l2-wrapper">
      <a href="<?php echo get_page_link(18); ?>">
        <h1 class="apo-koinou page-title page-title-text">
          <?php echo get_the_title(18); ?>
        </h1>
      </a>
    </div>
  </div>
  <h2 class="apo-koinou static-title title-text">Τι παρέχουμε</h2>

  <p class="apo-koinou content-text">Ως συλλογικότητα, αναγνωρίζουμε ότι υπάρχουν ομάδες ανθρώπων των οποίων οι φωνές και τα βιώματα αποσιωπούνται. Γι' αυτό το λόγο, μέσω του τρόπου δουλειάς μας επιδιώκουμε να δίνουμε χώρο σ' αυτές τις φωνές, συμβάλλοντας στην προσπάθεια για την άρση των κοινωνικών αποκλεισμών.</p>
  <p class="apo-koinou content-text">Επιπλέον, παρέχουμε υπηρεσίες διαβαθμισμένου κόστους, βάσει συγκεκριμένων κοινωνικών κριτηρίων και έπειτα από συνεννόηση με τους ανθρώπους που απευθύνονται σ' εμάς.</p>

  <h3 class="apo-koinou static-title title-text">1) Ψυχολογική υποστήριξη και ψυχοθεραπεία</h3>

  <p class="apo-koinou content-text">Ατομική (παιδιά, έφηβοι/ες, ενήλικες) και ομαδική ψυχοθεραπεία διαφορετικών προσεγγίσεων.</p>

  <h3 class="apo-koinou static-title title-text">2) Κοινωνική ενδυνάμωση και δράσεις</h3>

  <p class="apo-koinou content-text">Σχεδιασμός και εκπόνηση κοινοτικών προγραμμάτων με σκοπό την ευαισθητοποίηση και την ενδυνάμωση κοινοτήτων ή κοινωνικών ομάδων σε διάφορα θέματα. (λινκ)</p>

  <h3 class="apo-koinou static-title title-text">3) Εκπαιδευτικά σεμινάρια</h3>

  <p class="apo-koinou content-text">Απευθύνονται σε επαγγελματίες ψυχικής υγείας και συναφών κλάδων, καθώς και στο ευρύ κοινό. </p>

  <h3 class="apo-koinou static-title title-text">4) Ερευνητικό έργο</h3>

  <p class="apo-koinou content-text">Επιστημονικό έργο Από Κοινού, εστιασμένο σε ψυχοκοινωνικά ζητήματα.</p>
  <p class="apo-koinou content-text">Ανάληψη επαγγελματικών μεταφράσεων (αγγλικά-ελληνικά) επιστημονικού έργου που αφορά στους κλάδους κοινωνικών επιστημών και άλλους συναφείς κλάδους.</p>
  <p class="apo-koinou content-text">Ανάληψη επιστημονικών μελετών ή μέρους τους (π.χ. βιβλιογραφική ανασκόπηση, δημιουργία βάσης δεδομένων, συλλογή δεδομένων, εισαγωγή δεδομένων σε SPSS, στατιστική ανάλυση / ανάλυση ποιοτικών δεδομένων, κλπ.) για λογαριασμό άλλων.</p>
  <p class="apo-koinou content-text">Επιμέλειες επιστημονικών εργασιών ή συγγραμμάτων στους κλάδους των κοινωνικών επιστημών ή σε συναφείς κλάδους. </p>
  
  </section>
</div>
<?php get_footer();