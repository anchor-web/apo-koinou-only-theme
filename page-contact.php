<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

get_header();
?>

<section class="container">
<!-- Here lies the page title. -->
<div class="apo-koinou page-title l1-wrapper">
  <div class="apo-koinou page-title l2-wrapper">
    <a href="<?php echo get_page_link(51); ?>">
      <h1 class="apo-koinou page-title page-title-text">
        <?php echo get_the_title(51); ?>
      </h1>
    </a>
  </div>
</div>
</section>

<?php
get_footer();