<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Απο Κοινου -- Ψυχοκοινωνικη Ενδυναμωση & Ψυχοθεραπεια</title>
  </head>
  <body>
    <div class="gradient-wallpaper">
      <div class="pattern-wallpaper">
        <img src="" class="logo-image">
        <div class="title-text">
          <span>Ψυχοκοινωνική Ενδυνάμωση</span>
          <span>Ψυχοθεραπεία</span>
        </div>
        <div class="contact-data">
          <span>Θεοδωρήτου Βρεσθένης 9</span>
          <img src="" class="orange-dot">
          <span>Νέος Κόσμος</span>
          <img src="" class="orange-dot">
          <span>2109333333</span>
        </div>
      </div>
    </div>
  </body>
</html>