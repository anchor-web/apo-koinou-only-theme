<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php if (is_singular() && pings_open(get_queried_object())): ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url')?>">
    <?php endif; ?>
    <?php wp_head(); ?>
    
    <style>
      body{
        color: #4e7c71;
        position: relative;
      }
      .curtain.l1{
        height: 100vh;
        background: rgb(255,255,255);
        background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(135,186,176,1) 84%, rgba(106,146,138,1) 100%);
        background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%,rgba(135,186,176,1) 84%,rgba(106,146,138,1) 100%);
        background: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(135,186,176,1) 84%,rgba(106,146,138,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#6a928a',GradientType=0 );
      }
      .curtain.l2{
        height: 100vh;
        background-image: url("<?php echo wp_get_attachment_image_src(91, 'full')[0]; ?>");
      }
      .orange-ball-sep-small{
        width: 6px;
        height: 6px;
      }
      .orange-ball-sep-med{
        width: 9px;
        height: 9px;
      }
      img.ak-logo.logo-img{
        display: block;
        position: relative;
        width: 280px;
        height: 280px;
        margin-top: 40px;
        margin-left: auto;
        margin-right: auto;
      }
      #brand{
        padding-top: 40px;
        font-size: 95px;
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 600px;
        text-align: center;
        text-shadow: .3px 1px #666;
        letter-spacing: 7px;
      }
      #sub-brand{
        padding-top: 0px;
        font-size: 44px;
        display: block;
        margin: auto;
        width: 900px;
        text-align: center;
        letter-spacing: 2px;
        text-shadow: .3px .4px #666;
      }
      #sub-brand span{
        /*line-height: 36px;*/
        display: inline;
        padding-left: 9px;
        padding-right: 9px;
      }
      #comm{
        padding-top: 46px;
        font-size: 28px;
        display: block;
        margin-top: 60px;
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        text-align: center;
      }
      #comm span{
        padding-left: 15px;
        padding-right: 15px;
      }
    </style>
  </head>
  <body>
    <div class="curtain l1">
      <div class="curtain l2">
        <div class="container">
          <div class="row"><div class="col-xs-12">
          <a href="#<?php //echo get_home_url(); ?>"><img class="apo-koinou ak-logo logo-img" src="<?php echo wp_get_attachment_image_src(93, 'full'/* to add the logo size to the list of image sizes. */)[0]; ?>"></a>
          <div id="brand">"Από Κοινού"</div>
          <div id="sub-brand">
            <span>Ψυχοκοινωνική Ενδυνάμωση</span>
            <img class="orange-ball-sep-med" src="<?php echo wp_get_attachment_image_src(92, 'full')[0]; ?>">
            <span>Ψυχοθεραπεία</span>
          </div>
          <div id="comm">
            <span id="address">Θεοδωρήτου Βρεσθένης 9</span>
            <img class="orange-ball-sep-small" src="<?php echo wp_get_attachment_image_src(92, 'full')[0]; ?>">
            <span id="address">Νέος Κόσμος</span>
            <img class="orange-ball-sep-small" src="<?php echo wp_get_attachment_image_src(92, 'full')[0]; ?>">
            <span id="address">+30 215 555 3805</span>
          </div>
          </div></div>
        </div>
      </div>
    </div>
  </body>