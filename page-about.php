<?php

get_header();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!-- The begin of the posts section -->
<div class="post-section-wrapper">
  <section id="page-content-section" class="container">

  <!-- Here lies the page title. -->
  <div class="apo-koinou page-title l1-wrapper">
    <div class="apo-koinou page-title l2-wrapper">
      <a href="<?php echo get_page_link(16); ?>">
        <h1 class="apo-koinou page-title page-title-text">
          <?php echo get_the_title(16); ?>
        </h1>
      </a>
    </div>
  </div>


<h2 class="apo-koinou static-title title-text">Λίγα λόγια για εμάς</h2>

<p class="apo-koinou content-text">Το συνεργατικό εγχείρημα Από Κοινού είναι το αποτέλεσμα των προσπαθειών μιας ομάδας ανθρώπων που δραστηριοποιούνται στους τομείς της ψυχοκοινωνικής υποστήριξης, της κοινοτικής ενδυνάμωσης, της ψυχοθεραπείας, της έρευνας και των κοινωνικών επιστημών. Προέκυψε από την ανάγκη μας να δουλεύουμε με αξιοπρέπεια, οργανώνοντας από κοινού την εργασία μας σε ένα μη-καταπιεστικό, μη-ιεραρχικό πλαίσιο.</p>
<p class="apo-koinou content-text">Ως συλλογικότητα, προσπαθούμε να διερευνήσουμε τους τρόπους με τους οποίους θα μπορούσε να εφαρμοστεί στο δικό μας τομέα δουλειάς το αναδυόμενο παράδειγμα των συνεργατικών εγχειρημάτων.</p>
<p class="apo-koinou content-text">Ως ομάδα, αντλούμε από την πείρα, την εκπαίδευση και τις διαφορετικές οπτικές μας προς όφελος των ανθρώπων με τους οποίους δουλεύουμε, αφήνοντας χώρο για τα επιμέρους ενδιαφέροντα και την ανάπτυξή μας σε ατομικό και συλλογικό επίπεδο. </p>
<p class="apo-koinou content-text">Ως επαγγελματίες, αναζητούμε μοντέλα εργασίας που μπορούν να κάνουν την ψυχοθεραπεία και τα προγράμματα κοινοτικής ενδυνάμωσης προσβάσιμα σε περισσότερους ανθρώπους, μην αναπαράγοντας κοινωνικά προνόμια και αίροντας τους αποκλεισμούς (π.χ. το στίγμα, το υψηλό χρηματικό αντίτιμο κ.α.) που τα συνοδεύουν. </p>
	

<h2 class="apo-koinou static-title title-text">Που απευθυνόμαστε</h2>

<p class="apo-koinou content-text">Σε πρόσωπα, οικογένειες, ομάδες και οργανισμούς που αντιμετωπίζουν ψυχοκοινωνικές δυσκολίες.</p>
<p class="apo-koinou content-text">Σε επαγγελματίες (π.χ. ψυχικής υγείας, εκπαίδευσης, κ.α.) ή σε ομάδες επαγγελματιών.</p>
<p class="apo-koinou content-text">Σε άτομα ή κοινότητες που επιθυμούν να συμβάλουν σε προγράμματα κοινοτικής ενδυνάμωσης ή/και συμμετοχικής έρευνας δράσης.</p>

  </section>
</div>
<?php get_footer(); ?>