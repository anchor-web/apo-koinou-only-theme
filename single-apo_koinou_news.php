<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="post-section-wrapper">
	<section id="post-content-section" class="container">

  <!-- Here lies the page title complex (title and breadcrumbs). -->
    <div class="apo-koinou page-title l1-wrapper">
      <div class="apo-koinou page-title l2-wrapper">
        <a href="<?php echo get_site_url(); ?>">
          <h3 class="apo-koinou breadcrumbs breadcrumbs-text">
            Νεα "Α. Κ."
          </h3>
        </a>
      </div>
    </div>
    
			<?php
      /* Start the Loop */
      while ( have_posts() ) : the_post();
      ?>
  
    <!-- Here lies the article header -->
    <header class="apo-koinou single article-header">

      <!-- Here lies the featured article image -->
      <div class="apo-koinou single article-featured l2-wrapper">
        <a href="<?php the_permalink(); ?>">
          <?php
          if(has_post_thumbnail()):
              the_post_thumbnail('single_page_featured');
          else:
          ?>
            <img class="apo-koinou single article-featured featured-image" src="<?php echo wp_get_attachment_image_src(87, 'single_page_featured')[0]; ?>">
          <?php
          endif;
          ?>
        </a>
      </div>
      
      <!-- Here lies the title of the news -->
      <div class="apo-koinou single article-title l2-wrapper">
        <h1 class="apo-koinou page-title page-title-text">
          <?php the_title(); ?>
        </h1>
      </div>
      
      <!-- Here lies the date of the news -->
      <div class="apo-koinou single article-date l2-wrapper">
        <span class="apo-koinou single article-date date-text">
          <?php the_date(); ?>
        </span>
      </div>
      
      <!-- Here lies the category indicator for the news -->
      <div class="apo-koinou single category-indicator l2-wrapper">
        <div class="apo-koinou single category-indicator indicator-content <?php $key=array_keys(get_the_taxonomies(get_the_ID()))[0]; echo get_the_terms(get_the_ID(), $key)[0]->slug; ?>">
          <div class="apo-koinou single category-indicator little-square"></div>
          <div class="apo-koinou single category-indicator label-rectangle">
            <?php
            $args=array(
                "template"=>'%2$l',
                "term_template"=>'<span>%2$l</span>'
            );
            echo get_the_taxonomies(get_the_ID(), $args)[$key];
            ?>
          </div>
        </div>
      </div>
    </header>

      <!-- Here lies the news content-->
      <div class="apo-koinou article-content l1-wrapper">
        <div class="apo-koinou article-content l2-wrapper">
          <div class="apo-koinou article-content content-text">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
        
    <!-- Here lies the article edit link only for logged in users -->
      <?php
    if(is_user_logged_in()):
    ?>
    <div class="apo-koinou single article-edit-link l1-wrapper">
      <div class="apo-koinou single article-edit-link l2-wrapper">
        <div class="apo-koinou single article-edit-link edit-link">
          <?php edit_post_link(); ?>
        </div>
      </div>
    </div>
<?php
    endif;
    ?>
  </article>
        
      <?php
        /*the_post_navigation( array(
          'prev_text' => '<span class="screen-reader-text">' . 'previous post' . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'twentyseventeen' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">'.'</span>%title</span>',
          'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'twentyseventeen' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">'.'</span></span>',
        ));*/

				endwhile; // End of the loop.
			?>

		
	</section>
	<?php //get_sidebar(); ?>
</div>

<?php get_footer();
