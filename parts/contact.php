<!-- The contact elements section-->
<div class="section-wrapper">
  <section id="contact-section" class="container">
    <!-- The contact Bootstrap row-->
    <div class="row">
      <div class="col-sm-12 col-md-6 col-lg-4">
        <h2 class="apo-koinou contact-header">Από Κοινού</h2>
        <div id="address-details">
          <div>Θεοδωρήτου Βρεσθένης 9</div>
          <div>Νέος Κόσμος</div>
          <div>Τ.Κ. 11743</div>
        </div>
        <div id="communication-details">
          <div>Τηλέφωνο: +30 215 555 3805</div>
          <div>Ηλ. Αλληλογραφία: info@apo-koinou-cooperative.gr</div>
        </div>
        <h2 class="apo-koinou contact-header">Ώρες Λειτουργίας</h2>
        <div id="working-hours-details">
          <div>Δε-Πα: 11.00 - 19.00</div>
        </div>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-4">
        <h2 class="apo-koinou contact-header">Πρόσβαση</h2>
        <table id="access-details">
          <tr>
            <th>Τρόλει</th>
          </tr>
          <tr>
            <th>Γραμμη</th><th>Σταση</th>
          </tr>
          <tr>
            <td>10</td><td>Συγγρού-Φιξ/Αγ. Παντελεήμονας</td>
          </tr>
          <tr>
            <td>15</td><td>Αγ. Ιωάννης</td>
          </tr>
          <tr>
            <th>Λεωφορεία</th>
          </tr>
          <tr>
            <th>Γραμμη</th><th>Σταση</th>
          </tr>
          <tr>
            <td>040 (24ωρο), 550, Α2, Β2, Ε9, Ε90</td><td>Συγγρού-Φιξ</td>
          </tr>
          <tr>
            <td>106, 126, 136, 137</td><td>Φιξ</td>
          </tr>
          <tr>
            <td>550</td><td>Αγ. Παντελεήμων</td>
          </tr>
          <tr>
            <th>Μετρο</th>
          </tr>
          <tr>
            <th>Γραμμη</th><th>Σταση</th>
          </tr>
          <tr>
            <td>2 (Κόκκινη)</td><td>Συγγρού-Φιξ</td>
          </tr>
          <tr>
            <th>Τραμ</th>
          </tr>
          <tr>
            <th>Γραμμη</th><th>Σταση</th>
          </tr>
          <tr>
            <td>4, 5</td><td>Φιξ</td>
          </tr>
        </table>
      </div>
    </div>
  </section>
</div>