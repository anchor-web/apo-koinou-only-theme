<div class="apo-koinou masthead super-container">
  <div id="apo-koinou-masthead-container" class="apo-koinou section container">
    <div class="apo-koinou ak-menu-left-wrapper">
      <div class="apo-koinou ak-logo l2-wrapper">
        <a href="<?php echo get_home_url(); ?>"><img class="apo-koinou ak-logo logo-img" src="<?php echo wp_get_attachment_image_src(93, 'medium'/* to add the logo size to the list of image sizes. */)[0]; ?>"></a>
      </div>
    </div>
    <div class="apo-koinou ak-menu-right-wrapper">
      <div class="apo-koinou ak-site-title l2-wrapper">
        <div class="apo-koinou ak-site-title site-title-main">Από Κοινού</div>
        <div class="apo-koinou ak-site-title site-title-orange-dot"></div>
        <div class="apo-koinou ak-site-title site-title-sub">Ψυχοκοινωνική ενδυνάμωση & ψυχοθεραπεία</div>
      </div>
      <nav class="apo-koinou nav-menu nav-menu-inner-container">
        <?php wp_nav_menu(array('theme_location'=>'main_nav_menu')); ?>
      </nav>
    </div>
    <?php// echo get_stylesheet_directory().'/css/style.css'; ?>
  </div>
</div>
