<?php

/**
 * Register the Bootstrap libraries
 */
function register_bootstrap_system(){
  wp_enqueue_style('bootstrap_css', get_stylesheet_directory_uri().'/bootstrap/css/bootstrap.css');
  wp_register_script('bootstrap_js', get_stylesheet_directory_uri().'/bootstrap/js/bootstrap.js', array('jquery'), '3.3.7');
}
add_action('wp_enqueue_scripts', 'register_bootstrap_system');

/**
 * Register the style file
 */
function apo_koinou_enqueue_styles(){
  wp_enqueue_style('apo_koinou_main_style', get_stylesheet_directory_uri().'/css/style.css');
  //wp_enqueue_style('main_style', get_stylesheet_directory_uri().'/style.css');
}
add_action('wp_enqueue_scripts', 'apo_koinou_enqueue_styles');

function apo_koinou_enqueue_temp_style(){
  //if(is_home() || is_front_page()){
    wp_enqueue_style('apo_koinou_temp_style', get_stylesheet_directory_uri().'/css/temp-style.css');
  //}
}
add_action('wp_enqueue_scripts', 'apo_koinou_enqueue_temp_style');

/**
 * Register the menus
 */
function register_main_menu(){
  register_nav_menu('main_nav_menu', 'basic_menu');
}
add_action('init', 'register_main_menu');

/**
 * Register the custom thumbnail size for the archive pages
 */
function register_custom_image_sizes(){
  add_image_size("archive_page_thumb_crop", 570, 360, array('center', 'top'));
  add_image_size("single_page_featured", 1200, 730, array('center', 'center'));
//  add_image_size("archive_page_thumb");
}
add_action("after_setup_theme", "register_custom_image_sizes");

/**
 * ATTENTION: functionality to be removed due to the fact that there will be a 
 * simple page rendering past/coming events and NOT a taxonomy page.
 */

/**
 * Functionality for coming events
 *
 * This function hooks on the pre-get-post action only if the page is equal to 
 * page-coming events. It alters the main query so that all the coming events, 
 * according to the event's 'arranged-date' field, are retrieved from the
 * database and delivered to the page.
 */
function ak_retrieve_coming_events(){
  $query_args=array();
  //if(is_page(12)){
  $query=new WP_Query($query_args);
}
add_action('pre_get_posts', 'ak_retrieve_coming_events');

/**
 * Functionality for past events
 *
 * This function hooks on the pre-get-post action only if the page is equal to 
 * page-coming events. It alters the main query so that all the coming events, 
 * according to the event's 'arranged-date' field, are retrieved from the
 * database and delivered to the page.
 */
function ak_retrieve_past_events(){
  $query_args=array('field'=>'<current_date');
  $query=new WP_Query($query_args);
}
add_action('pre_get_posts', 'ak_retrieve_coming_events');

/**
 * Adding support for thumbnails.
 */
add_theme_support('post-thumbnails');
